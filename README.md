**Solution example - users list module with React**

Used: React Bootstrap, Formik, Yup

Design is simple but mobile friendly.

User details : 
 - firstname
 - surname
 - birthdate
 - email
 - personal foto

List of users showing : 
 - firstname
 - surname
 - age
 - email

 Users with age < 18 are in bold in table.

 Sorting is available on firstname, surname and age.

 Search is in one field for firstname, surname and email.

 Clicked on user in table the user details are showed.

 Adding and editing users is possible in detail and list.

 Delete user is possible only in detail. 

 Before deleting a confirmation alert is showed.

