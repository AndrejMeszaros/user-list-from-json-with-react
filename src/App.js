import UserList from "./Components/UserList";
import users from "../src/Data/response.json";

const AgeLimit = 18;

function App() {
  return (
    <div className="d-flex h-100 justify-content-center align-items-center p-2">
      <UserList users={users} ageLimit={AgeLimit} />
    </div>
  );
}

export default App;
