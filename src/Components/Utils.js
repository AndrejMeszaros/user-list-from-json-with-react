function FilterAndSort(users, sorting, sortedField, filter) {
  let filteredUsers = users.filter((user) => {
    if (user.firstname.includes(filter)) return user;
    if (user.surname.includes(filter)) return user;
    if (user.email.includes(filter)) return user;
    return null;
  });
  filteredUsers.sort((a, b) => {
    let aa, bb;
    if (sortedField === "firstname") {
      aa = a.firstname;
      bb = b.firstname;
    }
    if (sortedField === "surname") {
      aa = a.surname;
      bb = b.surname;
    }
    if (sortedField === "age") {
      aa = GetAge(a.birthdate);
      bb = GetAge(b.birthdate);
    }
    if (typeof aa === "string") {
      if (sorting === "asc") return aa > bb;
      else return aa < bb;
    }
    if (typeof aa === "number") {
      if (sorting === "asc") return aa - bb;
      else return bb - aa;
    }
    return null;
  });
  return filteredUsers;
}

function GetAge(birthdate) {
  if (typeof birthdate === "undefined") birthdate = "";
  var Age = 0;
  if (birthdate !== "") {
    var today = new Date();
    var today_year = today.getFullYear();
    var today_month = today.getMonth() + 1; // this function return [ 0-11]
    var today_day = today.getDate();
    var birthdate_year = parseInt(birthdate.substr(0, 4));
    var birthdate_month = parseInt(birthdate.substr(5, 2));
    var birthdate_day = parseInt(birthdate.substr(8, 2));
    Age = today_year - birthdate_year - 1;
    if (today_month > birthdate_month) {
      Age++;
    } else if (today_month === birthdate_month) {
      if (today_day >= birthdate_day) {
        Age++;
      }
    }
  }
  return Age;
}
export { FilterAndSort, GetAge };
