import { useState, useEffect } from "react";
import {
  Modal,
  Button,
  Form,
  Row,
  Col,
  Container,
  Image,
  Alert,
} from "react-bootstrap";
import { GetAge } from "./Utils";
import { Formik } from "formik";
import * as yup from "yup";

export default function UserDetails(props) {
  const [avatar, setavatar] = useState(props.user.avatar);
  const [confirmDelete, setConfirmDelete] = useState(false);

  /*eslint-disable */
  const schema = yup.object().shape({
    firstname: yup
      .string()
      .required()
      .matches(/^([\w\-]{3,20})$/),

    surname: yup
      .string()
      .required()
      .matches(/^([\w\-]{3,20})$/),

    birthdate: yup.date().max(new Date()).required(),

    email: yup
      .string()
      .email("Wrong email format !")
      .required("Email is requered !")
      .notOneOf(
        props.mode === "edit"
          ? []
          : props.users.map(function (user) {
              return user["email"];
            }),
        "Email already in use !"
      ),
  });
  /*eslint-enable */

  useEffect(() => {
    setavatar(props.user.avatar);
    setConfirmDelete(false);
  }, [props.mode, props.user]);

  function getReadOnly(field) {
    if (props.mode === "show") {
      if (field === "firstname") return true;
      if (field === "surname") return true;
      if (field === "birthdate") return true;
      if (field === "email") return true;
      if (field === "avatar") return true;
    }
    if (props.mode === "edit") {
      if (field === "firstname") return false;
      if (field === "surname") return false;
      if (field === "birthdate") return false;
      if (field === "email") return true;
      if (field === "avatar") return false;
    }
    if (props.mode === "new") {
      if (field === "firstname") return false;
      if (field === "surname") return false;
      if (field === "birthdate") return false;
      if (field === "email") return false;
      if (field === "avatar") return false;
    }
  }

  function handelAvatarChange(event) {
    setavatar(URL.createObjectURL(event.target.files[0]));
  }

  function handleSubmit(values) {
    props.userAction(values);
  }

  function handleDelete(values) {
    props.userAction(values);
  }

  return (
    <Modal
      show={props.show}
      onHide={props.onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      animation={false}
      backdrop="static"
      scrollable
      className=""
    >
      <Formik
        enableReinitialize={true}
        validationSchema={schema}
        onSubmit={(values) =>
          handleSubmit({
            firstname: values.firstname,
            surname: values.surname,
            birthdate: values.birthdate,
            email: values.email,
            avatar: avatar,
          })
        }
        initialValues={{
          firstname: props.user.firstname,
          surname: props.user.surname,
          birthdate: props.user.birthdate,
          email: props.user.email,
          avatar: props.user.avatar,
        }}
      >
        {({ handleSubmit, handleChange, values, touched, errors }) => (
          <Form
            noValidate
            onSubmit={handleSubmit}
            className="h-100 d-flex flex-column overflow-auto"
          >
            <Modal.Header
              closeButton
              style={{ backgroundColor: "rgba(0,0,0,.03)" }}
            >
              <Modal.Title className="text-center w-100">
                {props.mode === "show" ? "Show user details" : null}
                {props.mode === "edit" ? "Edit user details" : null}
                {props.mode === "new" ? "Add new user details" : null}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body className="p-3">
              <Container className="p-0 h-100">
                <Row className="m-0">
                  <Col
                    xs={12}
                    sm={6}
                    md={6}
                    lg={4}
                    className="d-flex justify justify-content-between flex-column"
                  >
                    <Container
                      className="mb-3 w-100 position-relative  pr-0 pl-0 "
                      style={{
                        position: "relative",
                        paddingTop: "133%",
                      }}
                    >
                      <Container
                        className="position-absolute p-0"
                        style={{ left: "0", right: "0", top: "0", bottom: "0" }}
                      >
                        <Image
                          rounded
                          src={avatar === "" ? "" : avatar}
                          className="w-100 h-100 "
                          style={{ objectFit: "cover" }}
                        />
                      </Container>
                    </Container>
                    <Form.Group controlId="avatar">
                      <Form.File
                        id="avatar"
                        custom
                        label="Select image"
                        accept="image/*"
                        name="avatar"
                        onChange={handelAvatarChange}
                        disabled={getReadOnly("avatar") ? true : false}
                      />
                    </Form.Group>
                  </Col>
                  <Col>
                    <Row className="h-100 align-content-between">
                      <Col lg={12}>
                        <Form.Group controlId="validationFormik01">
                          <Form.Label>First name</Form.Label>
                          <Form.Control
                            type="text"
                            name="firstname"
                            value={values.firstname}
                            onChange={handleChange}
                            isValid={touched.firstname && !errors.firstname}
                            isInvalid={!!errors.firstname}
                            readOnly={getReadOnly("firstname") ? true : false}
                            feedback="feedback"
                          />
                          <Form.Control.Feedback type="invalid">
                            Minimum 3, max 20 chars !
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Col>
                      <Col lg={12}>
                        <Form.Group controlId="validationFormik01">
                          <Form.Label>Surname</Form.Label>
                          <Form.Control
                            type="text"
                            name="surname"
                            value={values.surname}
                            onChange={handleChange}
                            isValid={touched.surname && !errors.surname}
                            isInvalid={!!errors.surname}
                            readOnly={getReadOnly("surname") ? true : false}
                          />
                          <Form.Control.Feedback type="invalid">
                            Minimum 3, max 20 chars !
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Col>
                      <Col lg={6} md={8} sm={8} xs={8}>
                        <Form.Group controlId="birthdate">
                          <Form.Label>Birthdate</Form.Label>
                          <Form.Control
                            type="date"
                            name="birthdate"
                            value={values.birthdate}
                            onChange={handleChange}
                            isValid={touched.birthdate && !errors.birthdate}
                            isInvalid={!!errors.birthdate}
                            readOnly={getReadOnly("birthdate") ? true : false}
                          />
                          <Form.Control.Feedback type="invalid">
                            Wrong date !
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Col>
                      <Col lg={6} md={4} sm={4} xs={4}>
                        <Form.Group controlId="age">
                          <Form.Label>Age</Form.Label>
                          <Form.Control
                            readOnly
                            value={GetAge(values.birthdate)}
                          />
                        </Form.Group>
                      </Col>
                      <Col lg={12}>
                        <Form.Group controlId="email">
                          <Form.Label>Email</Form.Label>
                          <Form.Control
                            type="text"
                            name="email"
                            value={values.email}
                            onChange={handleChange}
                            isValid={touched.email && !errors.email}
                            isInvalid={!!errors.email}
                            readOnly={getReadOnly("email") ? true : false}
                          />
                          <Form.Control.Feedback type="invalid">
                            {errors.email}
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Col>
                    </Row>
                    <Form.Row className="UserDeleteButton"></Form.Row>
                  </Col>
                </Row>
              </Container>
            </Modal.Body>
            <Modal.Footer
              className="pr-0 pl-0"
              style={{ backgroundColor: "rgba(0,0,0,.03)" }}
            >
              <Row className="w-100">
                <Col lg={6}></Col>
                {props.mode === "show" ? (
                  confirmDelete ? (
                    <Col>
                      <Alert
                        variant="danger"
                        onClose={() => setConfirmDelete(false)}
                        dismissible
                        className="mb-0"
                      >
                        <Alert.Heading>
                          <h6>Are you sure to delete this user ?</h6>
                        </Alert.Heading>
                        <Button
                          block
                          variant="danger"
                          onClick={() => handleDelete(values)}
                        >
                          Delete
                        </Button>
                      </Alert>
                    </Col>
                  ) : (
                    <>
                      <Col xs={4} lg={2}>
                        <Button
                          block
                          variant="outline-secondary"
                          onClick={() => props.modeChange("new")}
                        >
                          New
                        </Button>
                      </Col>
                      <Col xs={4} lg={2}>
                        <Button
                          block
                          variant="outline-secondary"
                          onClick={() => props.modeChange("edit")}
                        >
                          Edit
                        </Button>
                      </Col>
                      <Col xs={4} lg={2} className="">
                        <Button
                          block
                          variant="outline-secondary"
                          onClick={() => setConfirmDelete(true)}
                        >
                          Delete
                        </Button>
                      </Col>
                    </>
                  )
                ) : props.mode === "edit" ? (
                  <>
                    <Col xs={4} lg={2}></Col>
                    <Col xs={4} lg={2}></Col>
                    <Col xs={4} lg={2} className="">
                      <Button block variant="outline-secondary" type="submit">
                        Save
                      </Button>
                    </Col>
                  </>
                ) : props.mode === "new" ? (
                  <>
                    <Col xs={4} lg={2}></Col>
                    <Col xs={4} lg={2}></Col>
                    <Col xs={4} lg={2} className="">
                      <Button block variant="outline-secondary" type="submit">
                        Add
                      </Button>
                    </Col>
                  </>
                ) : null}
              </Row>
            </Modal.Footer>
          </Form>
        )}
      </Formik>
    </Modal>
  );
}
