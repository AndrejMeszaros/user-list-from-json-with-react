import { useEffect, useState } from "react";
import { FilterAndSort, GetAge } from "./Utils";
import {
  Card,
  Table,
  Container,
  Col,
  Row,
  InputGroup,
  FormControl,
  Button,
} from "react-bootstrap";
import UserDetails from "./UserDetails";

export default function UserList(props) {
  const [users, setUsers] = useState([]);
  const [sorting, setSorting] = useState("asc");
  const [filter, setFilter] = useState("");
  const [sortedField, setSortedField] = useState("");
  const [details, setDetails] = useState({
    show: false,
    user: { firstname: "", surname: "", birthdate: "", email: "", avatar: "" },
    mode: "",
  });

  function handleUserActions(user) {
    let newUsers = users;
    if (details.mode === "new") {
      newUsers.push(user);
    }
    if (details.mode === "edit") {
      newUsers = users.map((p) =>
        p.email === user.email
          ? {
              ...p,
              firstname: user.firstname,
              surname: user.surname,
              birthdate: user.birthdate,
              avatar: user.avatar,
            }
          : p
      );
    }
    if (details.mode === "show") {
      newUsers = users.filter(function (el) {
        return el.email !== user.email;
      });
    }
    setUsers(newUsers);
    handleCloseDetails();
  }

  function setSortedFieldAndSorting(field) {
    if (sortedField === field) {
      if (sorting === "asc") {
        setSorting("desc");
      } else setSorting("asc");
    } else {
      setSortedField(field);
    }
  }

  function addSortArrow(field) {
    if (sortedField === field) {
      if (sorting === "asc") {
        return <i className="bi bi-arrow-down"></i>;
      } else {
        return <i className="bi bi-arrow-up"></i>;
      }
    } else return null;
  }

  useEffect(() => {
    setUsers(props.users);
  }, [props.users]);

  function handleModeChange(mode) {
    if (mode === "new") {
      setDetails({
        ...details,
        show: true,
        user: {
          firstname: "",
          surname: "",
          birthdate: "",
          email: "",
          avatar: "",
        },
        mode: "new",
      });
    }
    if (mode === "edit") {
      setDetails({ ...details, show: true, mode: "edit" });
    }
  }

  function handleShowDetail(user) {
    setDetails({ ...details, show: true, user: user, mode: "show" });
  }

  function handleEditDetail(user) {
    setDetails({ ...details, show: true, user: user, mode: "edit" });
  }

  function handleAddDetail() {
    setDetails({
      ...details,
      show: true,
      user: {
        firstname: "",
        surname: "",
        birthdate: "",
        email: "",
        avatar: "",
      },
      mode: "new",
    });
  }

  function handleCloseDetails() {
    setDetails({ ...details, show: false, user: "", mode: "" });
  }

  return (
    <Card
      className="shadow-sm h-100 w-100"
      style={{ maxWidth: "1100px", maxHeight: "700px" }}
    >
      <UserDetails
        show={details.show}
        user={details.user}
        mode={details.mode}
        users={users}
        modeChange={handleModeChange}
        userAction={handleUserActions}
        onHide={() => {
          handleCloseDetails();
        }}
      />
      <Card.Header>
        <h2 className="m-0" style={{ textAlign: "center" }}>
          User list
        </h2>
      </Card.Header>
      <Card.Body className="h-100">
        <Container fluid className="h-100 w-100 p-0 m-0 d-flex border">
          <Container
            fluid
            className="d-block w-100  m-0 p-0 "
            style={{
              overflow: details.show ? "hidden" : "auto",
            }}
          >
            <Table
              striped
              bordered
              hover
              className="m-0"
              style={{
                borderSpacing: "0",
                borderCollapse: "separate",
                border: "0",
              }}
            >
              <thead>
                <tr>
                  <th
                    style={{ top: "0", backgroundColor: "white" }}
                    className="position-sticky align-top"
                    onClick={() => setSortedFieldAndSorting("firstname")}
                  >
                    First Name {addSortArrow("firstname")}
                  </th>
                  <th
                    style={{ top: "0", backgroundColor: "white" }}
                    className="position-sticky align-top"
                    onClick={() => setSortedFieldAndSorting("surname")}
                  >
                    Surname {addSortArrow("surname")}
                  </th>
                  <th
                    style={{ top: "0", backgroundColor: "white" }}
                    className="position-sticky align-top"
                    onClick={() => setSortedFieldAndSorting("age")}
                  >
                    Age {addSortArrow("age")}
                  </th>
                  <th
                    style={{ top: "0", backgroundColor: "white" }}
                    className="position-sticky align-top"
                  >
                    Email address
                  </th>
                  <th
                    style={{ top: "0", backgroundColor: "white" }}
                    className="position-sticky align-top"
                  >
                    Edit
                  </th>
                </tr>
              </thead>
              <tbody className="overflow-auto">
                {FilterAndSort(users, sorting, sortedField, filter).map(
                  (user, key) => (
                    <tr
                      key={key}
                      style={
                        GetAge(user.birthdate) < props.ageLimit
                          ? { fontWeight: "bold" }
                          : {}
                      }
                    >
                      <td
                        onClick={() => handleShowDetail(user)}
                        style={{ cursor: "pointer" }}
                      >
                        {user.firstname}
                      </td>
                      <td
                        onClick={() => handleShowDetail(user)}
                        style={{ cursor: "pointer" }}
                      >
                        {user.surname}
                      </td>
                      <td
                        onClick={() => handleShowDetail(user)}
                        style={{ cursor: "pointer" }}
                      >
                        {GetAge(user.birthdate)}
                      </td>
                      <td
                        onClick={() => handleShowDetail(user)}
                        style={{ cursor: "pointer" }}
                      >
                        {user.email}
                      </td>
                      <td>
                        <i
                          className="bi bi-pencil"
                          onClick={() => handleEditDetail(user)}
                          style={{ cursor: "pointer" }}
                        ></i>
                      </td>
                    </tr>
                  )
                )}
              </tbody>
            </Table>
          </Container>
        </Container>
      </Card.Body>
      <Card.Footer>
        <Row className="py-2 ">
          <Col sm={8} md={9}>
            <InputGroup className="m-0">
              <FormControl
                value={filter}
                placeholder="Seach in firstname, surname and email address..."
                onChange={(event) => setFilter(event.target.value)}
              />
              <InputGroup.Append>
                <InputGroup.Text
                  variant="secondary"
                  onClick={() => setFilter("")}
                >
                  <i className="bi bi-trash"></i>
                </InputGroup.Text>
              </InputGroup.Append>
            </InputGroup>
          </Col>
          <div className="d-block d-sm-none col-12 py-2"></div>
          <Col>
            <Button
              block
              variant="outline-secondary"
              onClick={() => {
                handleAddDetail();
              }}
            >
              Add new user
            </Button>
          </Col>
        </Row>
      </Card.Footer>
    </Card>
  );
}
